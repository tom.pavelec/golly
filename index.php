<?php

require_once ('Cell.php');
require_once ('Golly.php');
require_once ('vendor'.DIRECTORY_SEPARATOR.'autoload.php');

$golly = new golly\Golly();
$golly->createWorld();
$golly->initialPopulate();
$golly->iterate();
$golly->output();
